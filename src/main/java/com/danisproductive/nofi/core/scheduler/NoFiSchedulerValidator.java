package com.danisproductive.nofi.core.scheduler;

import com.danisproductive.nofi.core.scheduler.timed.TimedNoFiScheduler;
import com.danisproductive.nofi.core.scheduler.timed.TimedNoFiSchedulerValidator;
import com.danisproductive.nofi.core.utils.JsonValidationResult;
import com.danisproductive.nofi.core.utils.validation.Validator;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import static com.danisproductive.nofi.core.utils.json.adapter.process.NoFiProcessHierarchyTypeAdapter.CLASS_TYPE_PROPERTY;

public class NoFiSchedulerValidator {
    public static final String SCHEDULER_PROPERTY = "scheduler";
    public static final String SCHEDULER_TYPE_PROPERTY_PATH = SCHEDULER_PROPERTY + ": " + CLASS_TYPE_PROPERTY;
    private static final Map<String, Validator> schedulerValidators = new HashMap<>();

    static {
        schedulerValidators.put(TimedNoFiScheduler.class.getCanonicalName(), new TimedNoFiSchedulerValidator());
    }

    public static JsonValidationResult validate(JsonElement schedulerJson) {
        JsonValidationResult validationResult = new JsonValidationResult();

        if (schedulerJson.isJsonNull()) {
            validationResult.addInvalidPropertyValueError(SCHEDULER_PROPERTY);
        } else {
            if (!schedulerJson.isJsonObject()) {
                validationResult.addInvalidPropertyTypeError(SCHEDULER_PROPERTY, Object.class.getSimpleName());
            } else {
                JsonObject schedulerJsonObject = schedulerJson.getAsJsonObject();
                validateType(validationResult, schedulerJsonObject);
            }
        }

        return validationResult;
    }

    private static void validateType(JsonValidationResult validationResult, JsonObject schedulerJsonObject) {
        if (!schedulerJsonObject.has(CLASS_TYPE_PROPERTY)) {
            validationResult.addMissingPropertyError(SCHEDULER_TYPE_PROPERTY_PATH);
        } else {
            JsonElement schedulerType = schedulerJsonObject.get(CLASS_TYPE_PROPERTY);

            if (!schedulerType.getAsJsonPrimitive().isString()) {
                validationResult.addInvalidPropertyTypeError(SCHEDULER_TYPE_PROPERTY_PATH, String.class.getSimpleName());
            } else {
                String schedulerTypeValue = getTranslatedSchedulerType(schedulerType.getAsString());

                if (!schedulerValidators.containsKey(schedulerTypeValue)) {
                    validationResult.addInvalidPropertyValueError(SCHEDULER_TYPE_PROPERTY_PATH);
                } else {
                    validationResult.merge(schedulerValidators.get(schedulerTypeValue).validate(schedulerJsonObject));
                }
            }
        }
    }

    private static String getTranslatedSchedulerType(String schedulerType) {
        if (NoFiSchedulerDictionary.containsKey(schedulerType)) {
            return NoFiSchedulerDictionary.translate(schedulerType);
        }

        return schedulerType;
    }
}
