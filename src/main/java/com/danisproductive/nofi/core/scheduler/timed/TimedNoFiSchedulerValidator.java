package com.danisproductive.nofi.core.scheduler.timed;

import com.danisproductive.nofi.core.utils.JsonValidationResult;
import com.danisproductive.nofi.core.utils.ValidationResult;
import com.danisproductive.nofi.core.utils.validation.Validator;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class TimedNoFiSchedulerValidator implements Validator {
    private static final String INTERVAL_IN_MILLISECONDS_PROPERTY = "intervalInMilliseconds";

    @Override
    public ValidationResult validate(JsonElement schedulerJson) {
        JsonValidationResult validationResult = new JsonValidationResult();
        JsonObject schedulerJsonObject = schedulerJson.getAsJsonObject();

        if (!schedulerJsonObject.has(INTERVAL_IN_MILLISECONDS_PROPERTY)) {
            validationResult.addMissingPropertyError(INTERVAL_IN_MILLISECONDS_PROPERTY);
        } else {
            if (!schedulerJsonObject.get(INTERVAL_IN_MILLISECONDS_PROPERTY).getAsJsonPrimitive().isNumber()) {
                validationResult.addInvalidPropertyTypeError(INTERVAL_IN_MILLISECONDS_PROPERTY, "Long");
            }
        }

        return validationResult;
    }
}
