package com.danisproductive.nofi.core.utils;

@FunctionalInterface
public interface ClassCreator <T> {
    T create();
}
