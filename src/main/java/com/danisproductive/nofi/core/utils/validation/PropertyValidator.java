package com.danisproductive.nofi.core.utils.validation;

import com.danisproductive.nofi.core.utils.JsonValidationResult;
import com.google.gson.JsonElement;

@FunctionalInterface
public interface PropertyValidator {
    JsonValidationResult validate(JsonElement processAsJson);
}