package com.danisproductive.nofi.core.queue.kafka.config;

public class NoFiKafkaConsumerConfig {
    private String groupId;
    private int maxPollRecords;
    private boolean enableAutoCommit;
    private String autoOffsetReset;
    private String keyDeserializerClass;
    private String valueDeserializerClass;

    public NoFiKafkaConsumerConfig() {
        maxPollRecords = 1;
        enableAutoCommit = false;
        autoOffsetReset = "earliest";
        keyDeserializerClass = "org.apache.kafka.common.serialization.StringDeserializer";
        valueDeserializerClass = "org.apache.kafka.common.serialization.StringDeserializer";
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public int getMaxPollRecords() {
        return maxPollRecords;
    }

    public void setMaxPollRecords(int maxPollRecords) {
        this.maxPollRecords = maxPollRecords;
    }

    public boolean isEnableAutoCommit() {
        return enableAutoCommit;
    }

    public void setEnableAutoCommit(boolean enableAutoCommit) {
        this.enableAutoCommit = enableAutoCommit;
    }

    public String getAutoOffsetReset() {
        return autoOffsetReset;
    }

    public void setAutoOffsetReset(String autoOffsetReset) {
        this.autoOffsetReset = autoOffsetReset;
    }

    public String getKeyDeserializerClass() {
        return keyDeserializerClass;
    }

    public void setKeyDeserializerClass(String keyDeserializerClass) {
        this.keyDeserializerClass = keyDeserializerClass;
    }

    public String getValueDeserializerClass() {
        return valueDeserializerClass;
    }

    public void setValueDeserializerClass(String valueDeserializerClass) {
        this.valueDeserializerClass = valueDeserializerClass;
    }
}
