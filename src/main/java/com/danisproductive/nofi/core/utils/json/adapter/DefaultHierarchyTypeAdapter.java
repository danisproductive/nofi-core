package com.danisproductive.nofi.core.utils.json.adapter;

import com.danisproductive.nofi.core.utils.json.DefaultGson;
import com.google.gson.*;

import java.lang.reflect.Type;

import static com.danisproductive.nofi.core.utils.json.adapter.process.NoFiProcessHierarchyTypeAdapter.CLASS_TYPE_PROPERTY;

public class DefaultHierarchyTypeAdapter<T> implements JsonDeserializer<T>, JsonSerializer<T> {
    @Override
    public T deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String className = json.getAsJsonObject().get(CLASS_TYPE_PROPERTY).getAsString();
        try {
            return DefaultGson.gson.fromJson(json, (Type) Class.forName(className));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public JsonElement serialize(T src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject serializedObject = DefaultGson.gson.toJsonTree(src).getAsJsonObject();
        serializedObject.add(CLASS_TYPE_PROPERTY, new JsonPrimitive(src.getClass().getCanonicalName()));

        return serializedObject;
    }
}
