package com.danisproductive.nofi.core.server.http;

public class NoFiServerConfig {
    private Integer port;

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
}
