package com.danisproductive.nofi.core.utils.config;


import java.io.FileNotFoundException;

public interface ConfigWriter<T> {
    void save(T manager) throws FileNotFoundException;
}
