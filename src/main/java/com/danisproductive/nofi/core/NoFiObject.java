package com.danisproductive.nofi.core;

import java.util.UUID;

public class NoFiObject {
    private String id;

    public NoFiObject() {
        id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }
}
