package com.danisproductive.nofi.core.utils.config;

import java.io.FileNotFoundException;

public interface ConfigReader <T> {
    T read(String configFilePath, Class<? extends T> aClass) throws FileNotFoundException;
}
