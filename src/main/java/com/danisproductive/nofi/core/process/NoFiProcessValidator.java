package com.danisproductive.nofi.core.process;

import com.danisproductive.nofi.core.input.NoFiInputValidator;
import com.danisproductive.nofi.core.output.NoFiOutputValidator;
import com.danisproductive.nofi.core.scheduler.NoFiSchedulerValidator;
import com.danisproductive.nofi.core.utils.JsonValidationResult;
import com.danisproductive.nofi.core.utils.ValidationResult;
import com.danisproductive.nofi.core.utils.validation.PropertyValidator;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import static com.danisproductive.nofi.core.scheduler.NoFiSchedulerValidator.SCHEDULER_PROPERTY;

public class NoFiProcessValidator {
    private static final String NAME = "name";
    private static final String INPUTS = "inputs";
    private static final String OUTPUTS = "outputs";
    private static final String TYPE = "type";

    private static final Map<String, PropertyValidator> processPropertyValidators = new HashMap<>();

    static {
        processPropertyValidators.put(NAME, NoFiProcessValidator::isNameValid);
        processPropertyValidators.put(INPUTS, NoFiProcessValidator::areInputsValid);
        processPropertyValidators.put(OUTPUTS, NoFiProcessValidator::areOutputsValid);
        processPropertyValidators.put(SCHEDULER_PROPERTY, NoFiProcessValidator::isSchedulerValid);
//        processPropertyValidators.put(TYPE, NoFiProcessValidator::isTypeValid);
    }

    public static ValidationResult validate(JsonObject processAsJson) {
        JsonValidationResult validationResult = new JsonValidationResult();

        if (processAsJson != null) {
            processPropertyValidators.forEach((key, validator) -> {
                ValidationResult propertyValidation = validator.validate(processAsJson);
                validationResult.merge(propertyValidation);
            });
        } else {
            validationResult.addMissingPropertyError("config");
        }

        return validationResult;
    }

    private static JsonValidationResult isNameValid(JsonElement processAsJson) {
        JsonObject processAsJsonObject = processAsJson.getAsJsonObject();
        JsonValidationResult validationResult = new JsonValidationResult();

        if (processAsJsonObject.has(NAME)) {
            if (processAsJsonObject.get(NAME).isJsonNull()) {
                validationResult.addInvalidPropertyValueError(NAME);
            }
        } else {
            validationResult.addMissingPropertyError(NAME);
        }

        return validationResult;
    }

    private static JsonValidationResult isTypeValid(JsonElement jsonElement) {
        return null;
    }

    private static JsonValidationResult isSchedulerValid(JsonElement processAsJson) {
        JsonObject processAsJsonObject = processAsJson.getAsJsonObject();
        JsonValidationResult validationResult = new JsonValidationResult();

        if (processAsJsonObject.has(SCHEDULER_PROPERTY)) {
            validationResult = NoFiSchedulerValidator.validate(processAsJsonObject.get(SCHEDULER_PROPERTY));
        } else {
            validationResult.addMissingPropertyError(SCHEDULER_PROPERTY);
        }

        return validationResult;
    }

    private static JsonValidationResult areInputsValid(JsonElement processAsJson) {
        JsonObject processAsJsonObject = processAsJson.getAsJsonObject();
        return isArrayPropertyValid(processAsJsonObject, INPUTS, NoFiInputValidator::validate);
    }

    private static JsonValidationResult areOutputsValid(JsonElement processAsJson) {
        JsonObject processAsJsonObject = processAsJson.getAsJsonObject();
        return isArrayPropertyValid(processAsJsonObject, OUTPUTS, NoFiOutputValidator::validate);
    }

    private static JsonValidationResult isArrayPropertyValid(JsonObject processAsJson, String propertyName, PropertyValidator validator) {
        JsonValidationResult validationResult = new JsonValidationResult();

        if (processAsJson.has(propertyName)) {
            if (!processAsJson.get(propertyName).isJsonArray()) {
                JsonArray properties = processAsJson.get(propertyName).getAsJsonArray();

                for (JsonElement property : properties) {
                    JsonValidationResult propertyValidation = validator.validate(property);
                    validationResult.merge(propertyValidation);
                }
            }
        } else {
            validationResult.addMissingPropertyError(propertyName);
        }

        return validationResult;
    }
}
