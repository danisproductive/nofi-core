package com.danisproductive.nofi.core.server.websocket;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import static com.danisproductive.nofi.core.server.websocket.NoFiWebSocketMessageType.MESSAGE_TYPE_PROPERTY;

public class NoFiWebSocketClient<T> extends WebSocketClient {
    private T dataManager;
    private final Map<String, NoFiMessageProcessor<T>> messageProcessors;
    private final JsonParser parser;
    private AfterCallback afterCallback;

    public NoFiWebSocketClient(T dataManager) throws URISyntaxException {
        super(new URI("ws://localhost:3001"));
        this.dataManager = dataManager;
        messageProcessors = new HashMap<>();
        parser = new JsonParser();
    }

    public void setAfterCallback(AfterCallback afterCallback) {
        this.afterCallback = afterCallback;
    }

    public void addMessageProcessor(String messageName, NoFiMessageProcessor<T> processor) {
        messageProcessors.put(messageName, processor);
    }

    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        System.out.println("Connected to NoFiConnector");
    }

    @Override
    public void onMessage(String message) {
        JsonObject messageJson = parser.parse(message).getAsJsonObject();
        String messageName = messageJson.get(MESSAGE_TYPE_PROPERTY.getValue()).getAsString();

        if (messageProcessors.containsKey(messageName)) {
            messageProcessors.get(messageName).process(dataManager, messageJson);

            if (afterCallback != null) {
                afterCallback.after(message);
            }
        }
    }

    @Override
    public void onClose(int i, String s, boolean b) {
        System.out.println("Disconnected from NoFiConnector");
    }

    @Override
    public void onError(Exception e) {
        System.out.println("Error in NoFiConnector:\n" + e.getMessage());
    }

    public interface NoFiMessageProcessor<T> {
        void process(T dataManager, JsonObject data);
    }

    public interface AfterCallback {
        void after(String message);
    }
}
