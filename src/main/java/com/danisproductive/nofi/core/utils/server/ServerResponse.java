package com.danisproductive.nofi.core.utils.server;

public class ServerResponse {
    private int code;
    private String result;

    public ServerResponse(int code, String result) {
        this.code = code;
        this.result = result;
    }

    public int getCode() {
        return code;
    }

    public String getResult() {
        return result;
    }
}
