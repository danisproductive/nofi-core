package com.danisproductive.nofi.core.server.http;

import com.danisproductive.nofi.core.utils.config.FileConfigWriter;
import com.danisproductive.nofi.core.utils.json.DefaultGson;
import com.google.gson.Gson;
import io.javalin.Javalin;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public abstract class NoFiServer<T extends NoFiServerConfig> {
    private static final String PARAM_CONFIG = "config";

    protected T config;
    protected Javalin server;
    private FileConfigWriter<T> configWriter;

    public NoFiServer(String configPath, Class<T> aClass, Gson gson) {
        try {
            config = DefaultGson.gson.fromJson(new FileReader(configPath), aClass);
            configWriter = new FileConfigWriter<>(configPath, gson);
        } catch (IOException e) {
            System.err.println("No config file named: " + configPath + "found.\nExiting...");
            System.exit(1);
        }
    }

    public void start() {
        server = Javalin.create().start(config.getPort());

        setRoutes(server);
    }

    public void saveConfig() throws FileNotFoundException {
        configWriter.save(config);
    }

    protected abstract void setRoutes(Javalin server);
}
