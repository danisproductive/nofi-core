package com.danisproductive.nofi.core.process;

public class JavaNoFiProcess extends com.danisproductive.nofi.core.process.NoFiProcess {
    private String customProcessorClasspath;

    public String getCustomProcessorClasspath() {
        return customProcessorClasspath;
    }

    public void setCustomProcessorClasspath(String customProcessorClasspath) {
        this.customProcessorClasspath = customProcessorClasspath;
    }
}
