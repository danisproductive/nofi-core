package com.danisproductive.nofi.core.utils;

@FunctionalInterface
public interface ParameterClassCreator <K, V> {
    V create(K key);
}