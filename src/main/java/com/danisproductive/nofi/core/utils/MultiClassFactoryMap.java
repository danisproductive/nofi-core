package com.danisproductive.nofi.core.utils;

import java.util.HashMap;
import java.util.Map;


public class MultiClassFactoryMap <K, V> {
    Map<Class<? extends K>, ParameterClassCreator<? extends K,? extends V>> creators;

    public MultiClassFactoryMap() {
        creators = new HashMap<>();
    }

    public <T extends K, Y extends V> void put(Class<T> key, ParameterClassCreator<T, Y> value) {
        creators.put(key, value);
    }

    public <T extends K> ParameterClassCreator<? extends K, ? extends V> get(Class<T> key) {
        return creators.get(key);
    }

    @SuppressWarnings("unchecked")
    public V create(K value) {
        if (value != null) {
            // This cast is valid because the put function ensures the types match
            ParameterClassCreator<K, V> classCreator = (ParameterClassCreator<K, V>) creators.get(value.getClass());

            return classCreator.create(value);
        } else {
            return null;
        }
    }

    public <T extends K> boolean containsKey(Class<T> key) {
        return creators.containsKey(key);
    }
}
