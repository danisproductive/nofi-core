package com.danisproductive.nofi.core.queue.kafka;

import com.danisproductive.nofi.core.queue.NoFiQueue;
import com.danisproductive.nofi.core.queue.NoFiQueueFactory;
import com.danisproductive.nofi.core.queue.kafka.config.NoFiKafkaConfig;
import com.google.gson.JsonObject;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;

import static com.danisproductive.nofi.core.queue.NoFiQueueFactory.CONFIG_QUEUE_NAME;


public class KafkaNoFiQueueCreator implements NoFiQueueFactory.NoFiQueueCreator<NoFiKafkaConfig> {
    private static KafkaProducer<String, Object> kafkaProducer;
    private static KafkaConsumer<String, Object> kafkaConsumer;

    @Override
    public NoFiQueue<Object> create(JsonObject queueConfig, NoFiKafkaConfig kafkaConfig) {
        String topicName = queueConfig.get(CONFIG_QUEUE_NAME).getAsString();

        if (kafkaProducer == null || kafkaConsumer == null) {
            kafkaProducer = new KafkaProducer<>(kafkaConfig.getProducerProperties());
            kafkaConsumer = new KafkaConsumer<>(kafkaConfig.getConsumerProperties());
        }

        return new KafkaNoFiQueue<>(topicName, kafkaProducer, kafkaConsumer);
    }
}
