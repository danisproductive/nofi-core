package com.danisproductive.nofi.core.utils.json.adapter.process;

import com.danisproductive.nofi.core.process.NoFiProcess;
import com.danisproductive.nofi.core.queue.NoFiQueue;
import com.danisproductive.nofi.core.scheduler.NoFiScheduler;
import com.danisproductive.nofi.core.utils.json.adapter.DefaultHierarchyTypeAdapter;
import com.google.gson.*;
import java.lang.reflect.Type;


public class NoFiProcessHierarchyTypeAdapter implements JsonSerializer<NoFiProcess>, JsonDeserializer<NoFiProcess> {
    public static final String CLASS_TYPE_PROPERTY = "type";

    private final Gson processGson = new GsonBuilder()
            .registerTypeHierarchyAdapter(
                    NoFiQueue.class,
                    new DefaultHierarchyTypeAdapter<>()
            ).registerTypeHierarchyAdapter(
                    NoFiScheduler.class,
                    new DefaultHierarchyTypeAdapter<>()
            ).create();

    @Override
    public NoFiProcess deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String className = json.getAsJsonObject().get(CLASS_TYPE_PROPERTY).getAsString();
        try {
            return processGson.fromJson(json, (Type) Class.forName(className));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public JsonElement serialize(NoFiProcess src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject serializedObject = processGson.toJsonTree(src).getAsJsonObject();
        serializedObject.add(CLASS_TYPE_PROPERTY, new JsonPrimitive(src.getClass().getCanonicalName()));

        return serializedObject;
    }
}
