package com.danisproductive.nofi.core.scheduler;

import com.danisproductive.nofi.core.NoFiObject;
import com.google.gson.JsonObject;

import java.util.Comparator;

public abstract class NoFiScheduler extends NoFiObject {
    private int concurrentTasks;

    public int getConcurrentTasks() {
        return concurrentTasks;
    }

    public void setConcurrentTasks(int concurrentTasks) {
        this.concurrentTasks = concurrentTasks;
    }

    public abstract void setProperties(JsonObject schedulerConfig);

    @Override
    public boolean equals(Object scheduler) {
        if (scheduler instanceof NoFiScheduler) {
            NoFiScheduler schedulerCasted = (NoFiScheduler) scheduler;

            return concurrentTasks == schedulerCasted.concurrentTasks;
        }

        return false;
    }
}
