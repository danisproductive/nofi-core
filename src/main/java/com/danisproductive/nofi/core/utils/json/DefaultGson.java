package com.danisproductive.nofi.core.utils.json;

import com.google.gson.Gson;

public class DefaultGson {
    public static final Gson gson = new Gson();
}
