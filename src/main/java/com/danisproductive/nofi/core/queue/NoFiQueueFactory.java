package com.danisproductive.nofi.core.queue;

import com.danisproductive.nofi.core.queue.kafka.KafkaNoFiQueue;
import com.danisproductive.nofi.core.queue.kafka.KafkaNoFiQueueCreator;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Map;

import static com.danisproductive.nofi.core.server.websocket.NoFiWebSocketMessageProperty.QUEUE;

public class NoFiQueueFactory {
    private static final String CONFIG_QUEUE_TYPE = "type";
    public static final String CONFIG_QUEUE_NAME = "name";


    private static NoFiQueueFactory instance;
    private final Map<String, NoFiQueueCreator> queueCreators;

    private NoFiQueueFactory() {
        queueCreators = new HashMap<>();

        queueCreators.put(KafkaNoFiQueue.class.getCanonicalName(), new KafkaNoFiQueueCreator());
    }

    //todo redo this with annotations
//    private Map<String, NoFiQueueCreator> getQueueCreators() {
//        Reflections reflections = new Reflections(this.getClass().getPackage().getName());
//        Set<Class<? extends NoFiQueueCreator>> subTypes = reflections.getSubTypesOf(NoFiQueueCreator.class);
//        Map<String, NoFiQueueCreator> queueCreators = new HashMap<>();
//
//        subTypes.forEach((aClass) -> {
//            int mod = aClass.getModifiers();
//            if (!(Modifier.isAbstract(mod) || Modifier.isInterface(mod))) {
//                try {
//                    NoFiQueueCreator noFiQueueCreator = aClass.newInstance();
//                    //TODO create annotation to specify creator - config class relation instead of this
//                    String queueCreatorClassPath = aClass.getSimpleName();
//                    String queueConfigClassPath = "com.danisproductive.nofi.core.queue." + queueCreatorClassPath
//                            .substring(0, queueCreatorClassPath.length() - "Creator".length());
//                    queueCreators.put(queueConfigClassPath, noFiQueueCreator);
//                } catch (InstantiationException | IllegalAccessException e) {
//                    System.err.println("Failed to create instance for " + aClass.getCanonicalName() + " in order to create instances of that queue");
//                }
//
//
//            }
//        });
//
//        return queueCreators;
//    }

    public static NoFiQueueFactory getInstance() {
        if (instance == null) {
            instance = new NoFiQueueFactory();
        }

        return instance;
    }

    public <T> NoFiQueue create(JsonObject outputConfig, T queueServerConfig) {
        JsonObject queueConfigJson = outputConfig.get(QUEUE.getValue()).getAsJsonObject();
        String queueType = queueConfigJson.get(CONFIG_QUEUE_TYPE).getAsString();

        if (!queueType.contains(".")) {
            queueType = NoFiQueueDictionary.translate(queueType);
        }

        NoFiQueueCreator noFiQueueCreator = queueCreators.get(queueType);

        if (noFiQueueCreator != null) {
            return noFiQueueCreator.create(outputConfig, queueServerConfig);
        }

        return null;
    }

    public interface NoFiQueueCreator<T> {
        NoFiQueue<Object> create(JsonObject queueConfig, T serverConfig);
    }
}
