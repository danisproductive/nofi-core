package com.danisproductive.nofi.core.process;

import com.danisproductive.nofi.core.scheduler.timed.TimedNoFiScheduler;
import org.apache.kafka.common.utils.Java;

import java.util.HashMap;
import java.util.Map;

public class NoFiProcessDictionary {
    private static final Map<String, String> processDictionary = new HashMap<>();

    static {
        processDictionary.put(JavaNoFiProcess.class.getSimpleName(), JavaNoFiProcess.class.getCanonicalName());
    }

    public static String translate(String name) {
        return processDictionary.get(name);
    }

    public static boolean containsKey(String name) {
        return processDictionary.containsKey(name);
    }
}
