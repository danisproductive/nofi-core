package com.danisproductive.nofi.core.queue.kafka;

import com.danisproductive.nofi.core.queue.NoFiQueue;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.time.Duration;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

public class KafkaNoFiQueue<T> extends NoFiQueue<T> {
    private transient Producer<String, T> producer;
    private transient Consumer<String, T> consumer;

    public KafkaNoFiQueue(String topic, Producer<String, T> producer, Consumer<String, T> consumer) {
        super.setName(topic);
        this.producer = producer;
        this.consumer = consumer;
    }

    @Override
    public boolean push(T value) {
        producer.send(new ProducerRecord<>(super.getName(), value));
        producer.flush();
        return true;
    }

    @Override
    public T pull() {
        T value = peek();
        consumer.commitSync();

        return value;
    }

    @Override
    public T peek() {
        AtomicReference<T> value = new AtomicReference<>();
        consumer.subscribe(Collections.singletonList(super.getName()));
        ConsumerRecords<String, T> record = consumer.poll(Duration.ofMillis(1000));
        record.forEach((r -> value.set(r.value())));

        return value.get();
    }
}
