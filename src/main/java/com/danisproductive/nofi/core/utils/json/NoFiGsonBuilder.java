package com.danisproductive.nofi.core.utils.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class NoFiGsonBuilder {
    abstract protected void registerAdapters(GsonBuilder gsonBuilder);

    public Gson build() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        registerAdapters(gsonBuilder);

        return gsonBuilder.create();
    }
}
