package com.danisproductive.nofi.core.server.websocket;

import com.google.gson.JsonObject;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;

import static com.danisproductive.nofi.core.server.websocket.NoFiWebSocketMessageType.MESSAGE_TYPE_PROPERTY;

public class NoFiWebSocketServer extends WebSocketServer {
    public NoFiWebSocketServer(int port) {
        super(new InetSocketAddress(port));
    }

    @Override
    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        System.out.println("open " + clientHandshake.getResourceDescriptor());
    }

    @Override
    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        System.out.println("close");
    }

    @Override
    public void onMessage(WebSocket webSocket, String s) {
        System.out.println("message " + s);
    }

    @Override
    public void onError(WebSocket webSocket, Exception e) {
        System.out.println("error " + e.getMessage());
    }

    @Override
    public void onStart() {
        System.out.println("started");
    }

    public void broadcast(NoFiWebSocketMessageType messageType, JsonObject message) {
        message.addProperty(MESSAGE_TYPE_PROPERTY.getValue(), messageType.getValue());
        super.broadcast(message.toString());
    }
}
