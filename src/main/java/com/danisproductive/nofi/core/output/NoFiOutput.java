package com.danisproductive.nofi.core.output;

import com.danisproductive.nofi.core.NoFiObject;
import com.danisproductive.nofi.core.queue.NoFiQueue;

public class NoFiOutput extends NoFiObject {
    private String name;
    private NoFiQueue queue;

    public NoFiOutput(String name) {
        this.name = name;
    }

    public void setQueue(NoFiQueue queue) {
        this.queue = queue;
    }

    public NoFiQueue getQueue() {
        return queue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
