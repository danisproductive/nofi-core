package com.danisproductive.nofi.core.process;

import com.danisproductive.nofi.core.NoFiObject;
import com.danisproductive.nofi.core.input.NoFiInput;
import com.danisproductive.nofi.core.output.NoFiOutput;
import com.danisproductive.nofi.core.scheduler.NoFiSchedulerFactory;
import com.danisproductive.nofi.core.scheduler.NoFiScheduler;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.danisproductive.nofi.core.utils.json.adapter.process.NoFiProcessHierarchyTypeAdapter.CLASS_TYPE_PROPERTY;


public class NoFiProcess extends NoFiObject {
    private String name;
    private NoFiScheduler scheduler;
    private List<String> tags;
    private List<NoFiOutput> outputs;
    private List<NoFiInput> inputs;
    private boolean isRunning;
    private String executablePath;
    private String librariesPaths;

    public NoFiProcess() {
        tags = new ArrayList<>();
        outputs = new ArrayList<>();
        inputs = new ArrayList<>();
        isRunning = false;
    }

    public String getName() {
        return name;
    }

    public NoFiScheduler getScheduler() {
        return scheduler;
    }

    public List<String> getTags() {
        return tags;
    }

    public void removeTag(String tagName) {
        tags.remove(tagName);
    }

    public List<NoFiOutput> getOutputs() {
        return outputs;
    }

    public String getExecutablePath() {
        return executablePath;
    }

    public String getLibrariesPaths() {
        return librariesPaths;
    }

    public void setExecutablePath(String executablePath) {
        this.executablePath = executablePath;
    }

    public void setLibrariesPaths(String librariesPaths) {
        this.librariesPaths = librariesPaths;
    }

    public NoFiOutput getOutputByName(String outputName) {
        List<NoFiOutput> matchingOutputs = outputs.stream()
                .filter(output -> output.getName().equals(outputName))
                .collect(Collectors.toList());

        if (matchingOutputs.size() != 1) {
            throw new IllegalStateException("There is more then one output named: " + outputName +
                    "\n This should not happen, please rename one of the outputs");
        } else {
            return matchingOutputs.get(0);
        }
    }

    public void removeOutput(String outputName) {
        outputs.removeIf(output -> output.getName().equals(outputName));
    }

    public List<NoFiInput> getInputs() {
        return inputs;
    }

    public NoFiInput getInputByName(String inputName) {
        List<NoFiInput> matchingInputs = inputs.stream()
                .filter(input -> input.getName().equals(inputName))
                .collect(Collectors.toList());

        if (matchingInputs.size() != 1) {
            throw new IllegalStateException("There is more then one input named: " + inputName +
                    "\n This should not happen, please rename one of the inputs");
        } else {
            return matchingInputs.get(0);
        }
    }

    public void removeInput(String inputName) {
        inputs.removeIf(input -> input.getName().equals(inputName));
    }

    public void setName(String name) {
        this.name = name;
    }

    public void tag(String tagName) {
        tags.add(tagName);
    }

    public NoFiOutput addOutput(String outputName) {
        NoFiOutput newOutput = new NoFiOutput(outputName);
        outputs.add(newOutput);

        return newOutput;
    }

    public NoFiInput addInput(String inputName) {
        NoFiInput newInput = new NoFiInput(inputName);
        inputs.add(newInput);

        return newInput;
    }

    public <T extends NoFiScheduler> T setScheduler(JsonObject schedulerConfig) {
        scheduler = NoFiSchedulerFactory.getInstance().create(schedulerConfig.get(CLASS_TYPE_PROPERTY).getAsString());
        scheduler.setProperties(schedulerConfig);

        return (T) scheduler;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean running) {
        isRunning = running;
    }
}
