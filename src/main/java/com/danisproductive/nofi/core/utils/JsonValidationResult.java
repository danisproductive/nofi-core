package com.danisproductive.nofi.core.utils;

public class JsonValidationResult extends ValidationResult {
    public void addMissingPropertyError(String propertyName) {
        super.addMessage(String.format("Property %s is missing", propertyName));
        super.setValid(false);
    }

    public void addInvalidPropertyValueError(String propertyName) {
        super.addMessage(String.format("Property %s has invalid value", propertyName));
        super.setValid(false);
    }

    public void addInvalidPropertyTypeError(String propertyName, String expectedPropertyType) {
        super.addMessage(
                String.format("Property %s is of an invalid type. expected type is %s",
                        propertyName,
                        expectedPropertyType)
        );
        super.setValid(false);
    }
}
