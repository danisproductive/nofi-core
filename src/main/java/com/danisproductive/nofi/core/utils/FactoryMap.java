package com.danisproductive.nofi.core.utils;

import java.util.Map;

public class FactoryMap <T> {
    Map<Class<? extends T>, ClassCreator<? extends T>> creators;

    public <V extends T> void push(Class<V> key, ClassCreator<V> value) {
        creators.put(key, value);
    }

    public <V extends T> ClassCreator<? extends T> get(Class<V> key) {
        return creators.get(key);
    }
}
