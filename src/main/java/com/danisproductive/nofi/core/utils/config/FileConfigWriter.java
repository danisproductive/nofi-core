package com.danisproductive.nofi.core.utils.config;

import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class FileConfigWriter<T> implements ConfigWriter<T> {
    private final String configFilePath;
    private Gson gson;

    public FileConfigWriter(String configFilePath, Gson gson) {
        this.configFilePath = configFilePath;
        this.gson = gson;
    }

    @Override
    public void save(T data) throws FileNotFoundException {
        String dataJsonString = gson.toJson(data);

        try (PrintStream out = new PrintStream(new FileOutputStream(configFilePath))) {
            out.print(dataJsonString);
        }
    }
}
