package com.danisproductive.nofi.core.utils.validation;

import com.danisproductive.nofi.core.utils.ValidationResult;
import com.google.gson.JsonElement;

public interface Validator {
    ValidationResult validate(JsonElement jsonElement);
}
