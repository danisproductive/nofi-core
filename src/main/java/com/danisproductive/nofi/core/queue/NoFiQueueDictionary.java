package com.danisproductive.nofi.core.queue;

import com.danisproductive.nofi.core.queue.kafka.KafkaNoFiQueue;

import java.util.HashMap;
import java.util.Map;

public class NoFiQueueDictionary {
    private static final Map<String, String> queueDictionary = new HashMap<>();

    static {
        queueDictionary.put(KafkaNoFiQueue.class.getSimpleName(), KafkaNoFiQueue.class.getCanonicalName());
    }

    public static String translate(String name) {
        return queueDictionary.get(name);
    }

    public static boolean containsKey(String name) {
        return queueDictionary.containsKey(name);
    }
}
