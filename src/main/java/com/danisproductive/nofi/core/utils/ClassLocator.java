package com.danisproductive.nofi.core.utils;

import org.reflections.Reflections;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ClassLocator {
    public static <T> List<Class<? extends T>> getImplementingClasses(String classPath, Class<T> aClass) {
        Reflections reflections = new Reflections(classPath);
        Set<Class<? extends T>> subTypes = reflections.getSubTypesOf(aClass);
        List<Class<? extends T>> implementedSubTypes = new ArrayList<>();

        subTypes.forEach((subType) -> {
            int mod = subType.getModifiers();
            if (!(Modifier.isAbstract(mod) || Modifier.isInterface(mod))) {
                implementedSubTypes.add(subType);
            }
        });

        return implementedSubTypes;
    }
}

