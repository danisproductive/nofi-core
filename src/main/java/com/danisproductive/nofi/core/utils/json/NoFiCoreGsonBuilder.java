package com.danisproductive.nofi.core.utils.json;

import com.danisproductive.nofi.core.process.NoFiProcess;
import com.danisproductive.nofi.core.utils.json.adapter.process.NoFiProcessHierarchyTypeAdapter;
import com.google.gson.GsonBuilder;

public class NoFiCoreGsonBuilder extends NoFiGsonBuilder {
    @Override
    protected void registerAdapters(GsonBuilder gsonBuilder) {
        gsonBuilder.registerTypeHierarchyAdapter(
                NoFiProcess.class,
                new NoFiProcessHierarchyTypeAdapter()
        );
    }
}
