package com.danisproductive.nofi.core.queue;

import com.danisproductive.nofi.core.NoFiObject;

public abstract class NoFiQueue <T> extends NoFiObject {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract boolean push(T value);
    public abstract T pull();
    public abstract T peek();
}
