package com.danisproductive.nofi.core.scheduler;

import com.danisproductive.nofi.core.scheduler.timed.TimedNoFiScheduler;

import java.util.HashMap;
import java.util.Map;

public class NoFiSchedulerFactory {
    private final Map<String, SchedulerCreator<? extends NoFiScheduler>> schedulerFactories;
    private static NoFiSchedulerFactory _this;

    private NoFiSchedulerFactory() {
        schedulerFactories = new HashMap<>();
        initializeQueueFactories();
    }

    private void initializeQueueFactories() {
        schedulerFactories.put(
                TimedNoFiScheduler.class.getCanonicalName(),
                TimedNoFiScheduler::new
        );
        schedulerFactories.put(
                OnAvailabilityNoFiScheduler.class.getCanonicalName(),
                OnAvailabilityNoFiScheduler::new
        );
    }

    public static NoFiSchedulerFactory getInstance() {
        if (_this == null) {
            _this = new NoFiSchedulerFactory();
        }

        return _this;
    }

    public <T extends NoFiScheduler> T create(String schedulerClassPath) {
        if (!schedulerClassPath.contains(".")) {
            schedulerClassPath = NoFiSchedulerDictionary.translate(schedulerClassPath);
        }

        return (T) schedulerFactories.get(schedulerClassPath).create();
    }

    @FunctionalInterface
    private interface SchedulerCreator <T extends NoFiScheduler> {
        T create();
    }
}
