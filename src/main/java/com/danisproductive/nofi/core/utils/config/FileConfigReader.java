package com.danisproductive.nofi.core.utils.config;


import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class FileConfigReader <T> implements ConfigReader<T> {
    private Gson gson;

    public FileConfigReader(Gson gson) {
        this.gson = gson;
    }

    @Override
    public T read(String configFilePath, Class<? extends T> aClass) throws FileNotFoundException {
        return gson.fromJson(new FileReader(configFilePath), aClass);
    }
}
