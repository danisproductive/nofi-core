package com.danisproductive.nofi.core.input;

public class NoFiInputSource {
    private String sourceProcessId;
    private String sourceOutputName;

    public NoFiInputSource(String sourceProcessId, String sourceOutputName) {
        this.sourceProcessId = sourceProcessId;
        this.sourceOutputName = sourceOutputName;
    }
}
