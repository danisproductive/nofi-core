package com.danisproductive.nofi.core.queue.kafka.config;

public class NoFiKafkaProducerConfig {
    public String clientId;
    private String keySerializerClass;
    private String valueSerializerClass;

    public NoFiKafkaProducerConfig() {
        clientId = "asd";
        keySerializerClass = "org.apache.kafka.common.serialization.StringSerializer";
        valueSerializerClass = "org.apache.kafka.common.serialization.StringSerializer";
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getKeySerializerClass() {
        return keySerializerClass;
    }

    public void setKeySerializerClass(String keySerializerClass) {
        this.keySerializerClass = keySerializerClass;
    }

    public String getValueSerializerClass() {
        return valueSerializerClass;
    }

    public void setValueSerializerClass(String valueSerializerClass) {
        this.valueSerializerClass = valueSerializerClass;
    }
}
