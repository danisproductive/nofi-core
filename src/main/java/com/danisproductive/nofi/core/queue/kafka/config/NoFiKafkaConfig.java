package com.danisproductive.nofi.core.queue.kafka.config;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.DeleteTopicsResult;
import org.apache.kafka.clients.admin.KafkaAdminClient;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;

import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class NoFiKafkaConfig {
    private String bootstrapServers;
    private AdminClient adminClient;
    private NoFiKafkaConsumerConfig consumerConfig;
    private NoFiKafkaProducerConfig producerConfig;

    public NoFiKafkaConfig(String bootstrapServers, NoFiKafkaConsumerConfig consumerConfig, NoFiKafkaProducerConfig producerConfig) {
        this.bootstrapServers = bootstrapServers;
        this.consumerConfig = consumerConfig;
        this.producerConfig = producerConfig;
        adminClient = KafkaAdminClient.create(getAdminClientProperties());
    }

    private Properties getAdminClientProperties() {
        Properties adminClientProperties = new Properties();
        adminClientProperties.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);

        return adminClientProperties;
    }

    public Properties getConsumerProperties() {
        Properties consumerProperties = new Properties();
        consumerProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, consumerConfig.getGroupId());
        consumerProperties.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, consumerConfig.getMaxPollRecords());
        consumerProperties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, consumerConfig.isEnableAutoCommit());
        consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, consumerConfig.getAutoOffsetReset());
        consumerProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, consumerConfig.getKeyDeserializerClass());
        consumerProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, consumerConfig.getValueDeserializerClass());

        return consumerProperties;
    }

    public Properties getProducerProperties() {
        Properties producerProperties = new Properties();
        producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        producerProperties.put(ProducerConfig.CLIENT_ID_CONFIG, producerConfig.getClientId());
        producerProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, producerConfig.getKeySerializerClass());
        producerProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, producerConfig.getValueSerializerClass());

        return producerProperties;
    }

    public void deleteTopic(String... topicNames) throws ExecutionException, InterruptedException {
        DeleteTopicsResult deleteTopicsResult = adminClient.deleteTopics(Arrays.asList(topicNames));
        deleteTopicsResult.all().get();
    }
}
