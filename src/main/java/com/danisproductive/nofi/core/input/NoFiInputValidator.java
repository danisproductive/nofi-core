package com.danisproductive.nofi.core.input;

import com.danisproductive.nofi.core.utils.JsonValidationResult;
import com.google.gson.JsonElement;

public class NoFiInputValidator {
    private static final String INPUT = "input";

    public static JsonValidationResult validate(JsonElement inputAsJson) {
        JsonValidationResult validationResult = new JsonValidationResult();

        if (!inputAsJson.isJsonPrimitive() && !inputAsJson.getAsJsonPrimitive().isString()) {
            validationResult.addInvalidPropertyTypeError(INPUT, "String");
        }

        return validationResult;
    }
}
