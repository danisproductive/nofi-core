package com.danisproductive.nofi.core.server.websocket;

public enum NoFiWebSocketMessageType {
    MESSAGE_TYPE_PROPERTY("messageType"),
    PROCESS_ADDED("processAdded"),
    PROCESS_UPDATED("processUpdated"),
    PROCESS_DELETED("processDeleted"),
    PROCESS_DISCONNECTED("processDisconnected"),
    PROCESS_CONNECTED("processConnected"),
    PROCESS_STARTED("processStarted"),
    PROCESS_STOPPED("processStopped"),
    INPUT_ADDED("inputDeleted"),
    OUTPUTS_DELETED("outputsDeleted");

    private String value;

    NoFiWebSocketMessageType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
