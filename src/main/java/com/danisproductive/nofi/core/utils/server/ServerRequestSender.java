package com.danisproductive.nofi.core.utils.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ServerRequestSender {
    private String targetURL;

    public ServerRequestSender(String targetURL) {
        this.targetURL = targetURL;
    }

    public ServerResponse sendHttpsRequest(String route, String requestMethod, String requestBody) throws IOException {
        HttpURLConnection connection = getConnection(route, requestMethod);
        writeRequestBody(connection, requestBody);

        String requestResponseContent = "";
        if (requestBody.equalsIgnoreCase("GET")) {
            requestResponseContent = getRequestResult(connection.getInputStream());
        }

        return new ServerResponse(
            connection.getResponseCode(),
            requestResponseContent
        );
    }

    private HttpURLConnection getConnection(String route, String requestMethod) throws IOException {
        HttpURLConnection connection = (HttpURLConnection)new URL(targetURL + route).openConnection();
        connection.setRequestMethod(requestMethod);
        connection.setDoOutput(true);
        connection.setDoInput(true);

        connection.setRequestProperty("User-Agent", "Mozilla/5.0");

        return connection;
    }

    private void writeRequestBody(HttpURLConnection connection, String requestBody) throws IOException {
        DataOutputStream output = new DataOutputStream(connection.getOutputStream());
        output.writeBytes(requestBody);
        output.close();
    }

    private String getRequestResult(InputStream requestInputStream) throws IOException {
        DataInputStream input = new DataInputStream(requestInputStream);
        StringBuilder requestResult = new StringBuilder();
        int c;

        while ((c =input.read()) != -1)
            requestResult.append(c);
        input.close();

        return requestResult.toString();
    }

}
