package com.danisproductive.nofi.core.scheduler;

import com.danisproductive.nofi.core.scheduler.timed.TimedNoFiScheduler;

import java.util.HashMap;
import java.util.Map;

public class NoFiSchedulerDictionary {
    private static final Map<String, String> schedulerDictionary = new HashMap<>();

    static {
        schedulerDictionary.put(TimedNoFiScheduler.class.getSimpleName(), TimedNoFiScheduler.class.getCanonicalName());
    }

    public static String translate(String name) {
        return schedulerDictionary.get(name);
    }

    public static boolean containsKey(String name) {
        return schedulerDictionary.containsKey(name);
    }
}
