package com.danisproductive.nofi.core.output;

import com.danisproductive.nofi.core.utils.JsonValidationResult;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class NoFiOutputValidator {
    private static final String OUTPUT = "output";
    private static final String OUTPUT_NAME = "name";
    private static final String OUTPUT_QUEUE = "queue";
    private static final String OUTPUT_QUEUE_TYPE = "type";

    public static JsonValidationResult validate(JsonElement outputAsJson) {
        JsonValidationResult validationResult = new JsonValidationResult();

        if (!outputAsJson.isJsonObject()) {
            validationResult.addInvalidPropertyTypeError(OUTPUT, "Object");
        } else {
            JsonObject outputObject = outputAsJson.getAsJsonObject();
            validateOutputName(outputObject, validationResult);
            validateQueue(outputObject, validationResult);
        }

        return validationResult;
    }

    private static void validateOutputName(JsonObject outputObject, JsonValidationResult validationResult) {
        if (outputObject.has(OUTPUT_NAME)) {
            JsonElement outputName = outputObject.get(OUTPUT_NAME);

            if (!outputName.isJsonPrimitive() && !outputName.getAsJsonPrimitive().isString()) {
                validationResult.addInvalidPropertyTypeError(
                        OUTPUT + "_" + OUTPUT_NAME,
                        "String");
            }
        } else {
            validationResult.addMissingPropertyError(OUTPUT_NAME);
        }
    }

    private static void validateQueue(JsonObject outputObject, JsonValidationResult validationResult) {
        if (!outputObject.has(OUTPUT_QUEUE)) {
            validationResult.addMissingPropertyError(OUTPUT_QUEUE);
        } else {
            if (outputObject.get(OUTPUT_QUEUE).isJsonObject()) {
                JsonObject queueJson = outputObject.get(OUTPUT_QUEUE).getAsJsonObject();

                if (!queueJson.has(OUTPUT_QUEUE_TYPE)) {
                    JsonElement queueTypeElement = queueJson.get(OUTPUT_QUEUE_TYPE);
                    if (!queueTypeElement.isJsonPrimitive() && !queueTypeElement.getAsJsonPrimitive().isString()) {
                        validationResult.addInvalidPropertyValueError(OUTPUT_QUEUE_TYPE);
                    } else {
                        validationResult.addMissingPropertyError(OUTPUT_QUEUE_TYPE);
                    }
                } else {
                    validationResult.addInvalidPropertyTypeError(OUTPUT_QUEUE, "Object");
                }
            }
        }
    }
}
