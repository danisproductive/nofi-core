package com.danisproductive.nofi.core.input;

import com.danisproductive.nofi.core.NoFiObject;

public class NoFiInput extends NoFiObject {
    private String name;
    private NoFiInputSource source;

    public NoFiInput(String name) {
        this.name = name;
        source = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void connect(String sourceProcessId, String sourceOutputName) {
        source = new NoFiInputSource(sourceProcessId, sourceOutputName);
    }

    public void disconnect() {
        source = null;
    }
}
