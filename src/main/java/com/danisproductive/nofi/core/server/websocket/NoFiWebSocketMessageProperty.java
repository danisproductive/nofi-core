package com.danisproductive.nofi.core.server.websocket;

public enum NoFiWebSocketMessageProperty {
    PROCESS_CONFIG("processConfig"),
    PROCESS_ID("id"),
    PROCESS_OUTPUTS("outputs"),
    TARGET_PROCESS_ID("targetProcessId"),
    INPUT_NAME("inputName"),
    OUTPUT_NAME("outputName"),
    QUEUE("queue"),
    SOURCE_PROCESS_ID("sourceProcessId"),
    SOURCE_OUTPUT_NAME("sourceOutputName"),
    TARGET_INPUT_NAME("targetInputName");

    private String value;

    NoFiWebSocketMessageProperty(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
