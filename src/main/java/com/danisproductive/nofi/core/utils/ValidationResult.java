package com.danisproductive.nofi.core.utils;

public class ValidationResult {
    private String message;
    private boolean isValid;

    public ValidationResult() {
        message = "";
        isValid = true;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void addMessage(String message) {
        // Start new line only if an additional message was actually added
        if (!message.isEmpty() && !this.message.isEmpty()) {
            this.message += "\n";
        }

        this.message = this.message.concat(message);
    }

    public String getMessage() {
        return this.message;
    }

    public void merge(ValidationResult validationResult) {
        this.addMessage(validationResult.getMessage());

        if (!validationResult.isValid()) {
            isValid = false;
        }
    }
}
