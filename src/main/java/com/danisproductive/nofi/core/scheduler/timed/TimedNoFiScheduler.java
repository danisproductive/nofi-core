package com.danisproductive.nofi.core.scheduler.timed;

import com.danisproductive.nofi.core.scheduler.NoFiScheduler;
import com.google.gson.JsonObject;

public class TimedNoFiScheduler extends NoFiScheduler {
    private static final String CONFIG_INTERVAL = "intervalInMilliseconds";

    private Long intervalInMilliseconds;

    public void setInterval(long intervalInMilliseconds) {
        this.intervalInMilliseconds = intervalInMilliseconds;
    }

    public Long getIntervalInMilliseconds() {
        return intervalInMilliseconds;
    }

    @Override
    public void setProperties(JsonObject schedulerConfig) {
        setInterval(schedulerConfig.get(CONFIG_INTERVAL).getAsLong());
    }

    @Override
    public boolean equals(Object scheduler) {
        boolean isEqual = super.equals(scheduler);

        if (isEqual) {
            if (scheduler instanceof TimedNoFiScheduler) {
                TimedNoFiScheduler schedulerCasted = (TimedNoFiScheduler) scheduler;

                return intervalInMilliseconds.equals(schedulerCasted.intervalInMilliseconds);
            }
        }

        return false;
    }
}
