package com.danisproductive.nofi.core.tag;

import com.danisproductive.nofi.core.NoFiObject;

import java.util.ArrayList;
import java.util.List;

public class NoFiTag extends NoFiObject {
    private String name;
    private List<String> processIds;

    public NoFiTag(String name) {
        this.name = name;
        processIds = new ArrayList<>();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void tagProcess(String processId) {
        processIds.add(processId);
    }
}
